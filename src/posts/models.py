from django.db import models


def upload_location(instance, filename):
    return "%s/%s" % (instance.id, filename)


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=250)
    image = models.ImageField(null=True,
                              upload_to=upload_location,
                              blank=True,
                              width_field="image_width",
                              height_field="image_height")
    image_width = models.IntegerField(default=0)
    image_height = models.IntegerField(default=0)
    content = models.TextField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/posts/%s/" % (self.id)

    def get_edit_url(self):
        return "%s/edit/" % (self.id)

    class Meta:
        ordering = ["-timestamp", "-updated"]
